(** CYCLON: Inexpensive Membership Management for Unstructured P2P Overlays *)

(** {2 Cyclon_lwt} *)

(** High-level library implementing the CYCLON protocol using Lwt *)

open Cyclon

type 'data t

val init :
  View.key -> 'data -> 'data node View.t -> int -> int -> float
  -> ('data t -> View.key -> 'data -> 'data node View.t -> 'data node View.t Lwt.t)
  -> ('data t -> View.key -> 'data -> 'data node View.t -> 'data node View.t -> 'data node View.t Lwt.t)
  -> ('data t -> View.key -> 'data -> 'data node View.t -> unit Lwt.t)
  -> 'data t
(** [init my_nid my_ndata view view_len xchg_len period send_cb recv_cb view_cb]
    initializes a CYCLON instance with the following configuration:
    - [my_nid] - ID of this node
    - [my_ndata] - data associated with [my_nid] in view
    - [view] - map of neighbour entries with node ID as key
    - [view_len] - max view length
    - [xchg_len] - number of entries to exchange at each period
    - [period] - gossip period, in seconds
    - [send_cb nid ndata xchg] - send [xchg] entries to node [(nid, ndata)]
    - [recv_cb my_nid my_ndata view recvd] - called after receiving entries
      during an exchange; allows rewriting [recvd] entries with the returned value,
      thus allows using a stream sampler to provide uniformly random nodes
      instead of the possibly biased exchanged nodes
    - [view_cb my_nid my_ndata view] - called after the view has been updated
 *)

val view :
  'data t
  -> 'data node View.t
(** [view t]
    retrieve current view *)

val run :
  'data t
  -> unit Lwt.t
(** [run t send_cb recv_cb view_cb]
    run initiator:
    pick a random node from [view] to gossip with every [period] seconds
 **)

val recv :
  'data t -> View.key -> 'data -> 'data node View.t
  -> 'data node View.t Lwt.t
(** [recv t rnid rndata recvd send_cb recv_cb]
    receives a gossip message from a node [(rnid, rndata)] and sends a response;
    runs [recv_cb] with the received entries, and
    returns updated view *)
