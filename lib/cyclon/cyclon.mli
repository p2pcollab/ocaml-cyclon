(** CYCLON: Inexpensive Membership Management for Unstructured P2P Overlays *)

(** {2 Cyclon} *)

(** Low-level library providing reusable functions implementing
    the CYCLON protocol logic *)

type 'data node =
  {
    age: int;
    data: 'data;
  }
(** a node's profile:
    - [age]: age of this node profile, incremented after each gossip round
    - [data]: application-specific data associated with the node
 *)

module View : module type of Map.Make(String)

val add :
  View.key -> 'data -> 'data node View.t
  -> 'data node View.t
(** [add nid data view]
    adds a node with ID [nid] and associated [data] to [view] and
    returns the updated view *)

val remove :
  View.key -> 'data node View.t
  -> 'data node View.t
(** [remove nid view]
    removes the node with ID [nid] from [view] and
    returns the updated view *)

val make_exchange :
  'data node View.t
  -> View.key
  -> 'data
  -> int
  -> View.key option * 'data option * 'data node View.t * 'data node View.t
(** [make_exchange view my_nid my_data xchg_len]
   selects a random peer Q from [view] to exchange with
   and a list of nodes from [view] to send to Q,
   [my_nid] and [my_data] is the ID and data associated with this node,
   [xchg_len] is the number of nodes in the gossip exchange

   returns [(nid, data, xchg, view)] where:
   - [nid] is the node ID to exchange with,
   - [data] is associated with [nid] in [view],
   - [xchg] contains [xchg_len] nodes from [view] to send during the exchange,
   - [view] is the updated view with the age of all nodes increased
            and node [nid] removed *)

val make_response :
  'data node View.t
  -> int
  -> View.key
  -> 'data node View.t
(** [make_response view xchg_len rnid]
    responds to a gossip exchange initiated by [(rnid, rndata)]

    returns a list of nodes of length [xchg_len] from [view]
    to be sent as a response to [rnid]
 *)

val merge_recvd :
  'data node View.t
  -> int
  -> View.key
  -> 'data node View.t
  -> 'data node View.t
  -> int
  -> 'data node View.t
(** [merge_recvd view view_len my_nid sent recvd xchg_len]
    merges [recvd] entries received during an exchange
    to [view] of length [view_len];
    [sent] are the entries sent previously when initiating the exchange

    returns merged view *)
