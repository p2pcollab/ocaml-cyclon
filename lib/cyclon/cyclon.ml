type 'a node =
  {
    age: int;
    data: 'a;
  }

module View = Map.Make(String)

let add nid data view =
  View.add nid {age = 0; data} view

let remove nid view =
  View.remove nid view

(** pick a random node from [view] and
    return [(nid, node, view)]
    where view is the view with node removed *)
let random_node view =
  let view_len = View.cardinal view in
  if 0 < view_len then
    let (rnid, rnode, view, _, _) =
      View.fold
        (fun nid node a ->
          let (rnid, rnode, view, r, n) = a in
          if (n = r)
          then (Some nid, Some node, view, r, n + 1)
          else (rnid, rnode, View.add nid node view, r, n + 1))
        view
        (None, None, View.empty, Random.int (View.cardinal view), 0)
    in
    (rnid, rnode, view)
  else
    (None, None, view)

(** return [len] random nodes from [view] *)
let rec random_nodes ?(nodes = View.empty) view len =
  if 0 < len then
    match random_node view with
    | (Some rnid, Some rnode, view) ->
       let nodes = View.add rnid rnode nodes in
       random_nodes view (len - 1) ~nodes
    | _ -> nodes
  else nodes

(** increase age of all entries in view *)
let inc_age view =
  View.mapi (fun _nid ent -> {ent with age = ent.age + 1}) view

(** retrieve oldest node from [view],
    in case there are multiple oldest nodes of the same age,
    return a random one of those

    return [Some (nid, node, c)] where
    [nid], [node] is a random oldest node,
    [c] is the number of oldest entries *)

let oldest view =
  View.fold
    (fun nid node max ->
      match max with
      | None ->
         Some (nid, node, 1)
      | Some (_id, n, _c) when n.age < node.age ->
         Some (nid, node, 1)
      | Some (id, n, c) when n.age = node.age ->
         if Random.float 1. < 1. /. float_of_int (c + 1)
         then Some (nid, node, c + 1)
         else Some (id, n, c + 1)
      | _ -> max)
    view None

(** retrieve a node to exchange with and a list of nodes to send from [view],
    [my_nid] & [my_data] is the entry associated with this node,
    [xchg_len] is the number of entries in the exchange

    return [(Some nid, Some data, xchg, view)] where
    [nid] is the peer_id to exchange with
    [data] is associated with [peer_id] in [view]
    [xchg] contains [xchg_len] entries from view to send during an exchange
    [view] is the updated view with the age of all entries increased
           and the entry associated with [nid] removed *)
let make_exchange view my_nid my_data xchg_len =
  match oldest view with
  | Some (max_k, max_v, _max_n) ->
     let view = (View.remove max_k view) in
     (Some max_k,
      Some max_v.data,
      add my_nid my_data
        (random_nodes view (xchg_len - 1)),
      inc_age view)
  | None ->
     (None, None, View.empty, view)

(** generate response to an exchange request *)
let make_response view xchg_len rnid =
  random_nodes (View.remove rnid view) xchg_len

(** merge received entries with current view *)
let rec merge view view_len sent recvd xchg_len =
  if 0 < xchg_len && 0 < View.cardinal recvd then
    match random_node recvd with
    | (Some rnid, Some rnode, recvd) ->
       if View.cardinal view < view_len then
         (* fill an empty slot in view *)
         let view = View.add rnid rnode view in
         merge view view_len sent recvd (xchg_len - 1)
       else (* replace a sent entry in view with a received one *)
         (match random_node sent with
          | (Some snid, Some _sdata, sent) ->
             let view = View.remove snid view in
             let view = View.add rnid rnode view in
             merge view view_len sent recvd (xchg_len - 1)
          | _ -> view)
    | _ -> view
  else view

(** merge nodes received during an exchange with current view *)
let merge_recvd view view_len my_nid sent recvd xchg_len =
  (* remove own entry *)
  let sent = View.remove my_nid sent in
  let recvd = View.remove my_nid recvd in
  (* remove entries that already exist in view *)
  let recvd = View.filter (fun k _v -> not (View.mem k view)) recvd in
  merge view view_len sent recvd xchg_len
