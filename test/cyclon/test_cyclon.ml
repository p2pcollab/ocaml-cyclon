open Cyclon
open OUnit2
open Printf

let view_len = 7
let xchg_len = 3
let my_nid = "ME"
let my_data = 99

let print_view msg view =
  Printf.printf "\n%s\n" msg;
  View.iter (fun id n -> Printf.printf "%s: %d (%d)\n" id n.data n.age) view;;

let print_xchg msg xchg =
  Printf.printf "\n%s\n" msg;
  View.iter (fun id n -> Printf.printf "%s: %d\n" id n.data) xchg;;

let opt2str v =
  match v with
  | Some v -> v
  | None -> "-"

let opt2int v =
  match v with
  | Some v -> v
  | None -> -1

let fill_view =
  add "a" 1
    (add "b" 2
       (add "c" 3
          (add "d" 4
             (add "e" 5
                (add "f" 6
                   (add "g" 7
                      View.empty))))))

let fill_recvd =
    add "A" 10
      (add "B" 20
         (add "C" 30
            View.empty))

let test_add _ctx =
  let view = fill_view in
  assert_equal (View.cardinal view) 7

let test_xchg _ctx =
  let view = fill_view in
  let (nid, data, sent, _view) = make_exchange view my_nid my_data xchg_len in
  printf "\nSEND TO %s (%d)\n" (opt2str nid) (opt2int data);
  print_xchg "SEND:" sent;
  print_newline ();
  assert_equal (View.cardinal sent) xchg_len;
  assert_equal (View.mem "ME" sent) true

let test_recv _ctx =
  let view = fill_view in
  let (nid, data, sent, view) = make_exchange view my_nid my_data xchg_len in
  let recvd = fill_recvd in
  printf "\n\nSEND TO %s (%d)\n" (opt2str nid) (opt2int data);
  print_view "VIEW BEFORE:" view;
  print_xchg "SENT:" sent;
  print_xchg "RECVD:" recvd;
  assert_equal (View.cardinal sent) xchg_len;
  let view2 = merge_recvd view view_len my_nid sent recvd xchg_len in
  print_view "VIEW AFTER:" view2;
  assert_equal (View.cardinal view2) view_len;
  assert_equal (View.mem "ME" view2) false;
  assert_equal
    (View.cardinal (View.filter (fun k _v -> View.mem k view) view2))
    (view_len - xchg_len);
  assert_equal
    (View.cardinal (View.filter (fun k _v -> View.mem k recvd) view2))
    (xchg_len);
  let resp = make_response view2 xchg_len "A" in
  print_xchg "RESP:" resp;
  assert_equal (View.cardinal resp) xchg_len

let suite =
  "suite">:::
    [
      "add">:: test_add;
      "exchange">:: test_xchg;
      "receive">:: test_recv;
    ]

let () =
  Random.self_init ();
  run_test_tt_main suite
