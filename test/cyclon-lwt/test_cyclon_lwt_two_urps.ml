open Cyclon
open Urps

module C = Cyclon
module CL = Cyclon_lwt

let view_len = 8
let xchg_len = 4
let period = 1.0

let print_view nid msg view =
  Lwt.ignore_result (Lwt_io.printf "%s # view: %s (%d)\n"
                       nid msg (C.View.cardinal view));
  C.View.iter (fun id n ->
      Lwt.ignore_result (Lwt_io.printf " - %s: %s (%d)\n%!" id n.data n.age))
    view

let print_xchg nid msg xchg =
  Lwt.ignore_result (Lwt_io.printf "%s # xchg: %s (%d)\n"
                       nid msg (C.View.cardinal xchg));
  C.View.iter (fun id n ->
      Lwt.ignore_result (Lwt_io.printf " - %s: %s\n%!" id n.data))
    xchg

let rec read_ch ch c pid rpid rdata =
  let%lwt recvd = Lwt_io.read_value ch in
  print_xchg pid "read_ch" recvd;
  let%lwt view = CL.recv c rpid rdata recvd in
  print_view pid "read_ch" view;
  read_ch ch c pid rpid rdata

let () =
  let (read_ch1, write_ch2) = Lwt_io.pipe () in
  let (read_ch2, write_ch1) = Lwt_io.pipe () in

  let c = 700 in
  let s = 10 in
  let k = 50 in
  let idlen = 16 in

  let smpl1 = Sampler.init c s k idlen in
  let pid1 = "ABC123" in
  let data1 = "100" in
  let view1 = C.add "v1k1" "110"
                (C.add "v1k2" "120"
                   (C.add "v1k3" "130"
                      (C.add "v1k4" "140"
                         (C.add "v1k5" "150"
                            (C.add "v1k6" "160"
                               (C.add "v1k7" "170"
                                  C.View.empty))))))
  in
  let c1 =
    CL.init pid1 data1 view1 view_len xchg_len period
      (fun _c pid data entries ->
        let%lwt _ = Lwt_io.printf "%s # send_cb: %s (%s)\n" pid1 pid data in
        print_xchg pid1 "send_cb: entries to send" entries;
        let%lwt _ = Lwt_io.write_value write_ch1 entries in
        Lwt_io.read_value read_ch1)
      (fun _c my_pid _my_data _my_view recvd ->
        let sampled =
        View.fold (fun id data sampled ->
            let (k, d) = Sampler.add smpl1 id data in
            View.add k d sampled)
          recvd
          View.empty in
        print_xchg my_pid "recvd" recvd;
        print_xchg my_pid "sampled" sampled;
        Lwt.return sampled)
      (fun _c my_pid _my_data my_view ->
        print_view my_pid "updated" my_view;
        Lwt.return_unit)
  in

  let smpl2 = Sampler.init c s k idlen in
  let pid2 = "DEF456" in
  let data2 = "200" in
  let view2 = C.add "v2k1" "210"
                (C.add "v2k2" "220"
                   (C.add "v2k3" "230"
                      (C.add "v2k4" "240"
                         (C.add "v2k5" "250"
                            (C.add "v2k6" "260"
                               (C.add "v2k6" "270"
                                  C.View.empty))))))
  in
  let c2 =
    CL.init pid2 data2 view2 view_len xchg_len period
      (fun _c pid data entries ->
        let%lwt _ = Lwt_io.printf "%s # send_cb: %s (%s)\n" pid2 pid data in
        print_xchg pid2 "send_cb: entries to send" entries;
        let%lwt _ = Lwt_io.write_value write_ch2 entries in
        Lwt_io.read_value read_ch2
      )
      (fun _c my_pid _my_data _my_view recvd ->
        let sampled =
        View.fold (fun id data sampled ->
            let (k, d) = Sampler.add smpl2 id data in
            View.add k d sampled)
          recvd
          View.empty in
        print_xchg my_pid "recvd" recvd;
        print_xchg my_pid "sampled" sampled;
        Lwt.return sampled)
      (fun _c my_pid _my_data my_view ->
        print_view my_pid "after round" my_view;
        Lwt.return_unit)
  in
  let timeout = Lwt_unix.sleep 5.5
  in
  Random.self_init ();
  Lwt_main.run @@
    Lwt.pick [ CL.run c1;
               CL.run c2;
               read_ch read_ch1 c1 pid1 pid2 data2;
               read_ch read_ch2 c2 pid2 pid1 data1;
               timeout ]
