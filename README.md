[![Build Status](https://travis-ci.org/p2pcollab/ocaml-cyclon.svg?branch=master)](https://travis-ci.org/p2pcollab/ocaml-cyclon)

# CYCLON: Inexpensive Membership Management for Unstructured P2P Overlays

Cyclon is an OCaml implementation of the CYCLON protocol as specified in the
[paper](https://www.distributed-systems.net/my-data/papers/2005.jnsm.pdf).

Cyclon is distributed under the MPL-2.0 license.

## Protocol

1. Increase by one the age of all neighbors.
2. Select neighbor *Q* with the highest age among all neighbors, and *l - 1* other random neighbors.
3. Replace *Q*'s entry with a new entry of age 0 and with *P*'s address.
4. Send the updated subset to peer *Q*.
5. Receive from *Q* a subset of no more that *i* of its own entries.
6. Discard entries pointing at *P* and entries already contained in *P*'s cache.
7. Update *P*'s cache to include *all* remaining entries, by *firstly* using empty cache slots (if any),
   and *secondly* replacing entries among the ones sent to *Q*.

_Quoted from the paper above._

## Installation

``cyclon`` and `cyclon-lwt` can be installed via `opam`:

    opam install cyclon
    opam install cyclon-lwt

## Building

To build from source, generate documentation, and run tests, use `dune`:

    dune build
    dune build @doc
    dune runtest -f -j1 --no-buffer

In addition, the following `Makefile` targets are available
 as a shorthand for the above:

    make all
    make build
    make doc
    make test

## Documentation

The documentation and API reference is generated from the source interfaces.
It can be consulted [online][doc] or via `odig`:

    odig doc cyclon
    odig doc cyclon-lwt

[doc]: https://p2pcollab.github.io/doc/ocaml-cyclon/
